const blockDemo = require("./../../src");
const { defaultTheme } = require("@vuepress/theme-default");
const { searchPlugin } = require('@vuepress/plugin-search')
const { viteBundler } = require("@vuepress/bundler-vite");
const { path } = require('@vuepress/utils')

module.exports = {
  title: 'Avue',
  description: '前端搬砖神器,让数据驱动视图,更加贴合企业开发',
  templateDev: './docs/.vuepress/templates/dev.html',
  templateBuild: './docs/.vuepress/templates/build.html',
  bundler: viteBundler({}),
  theme: defaultTheme({
    darkmode: 'disable',
    anchorDisplay: false,
    colorModeSwitch: false,
    logo: '/images/logo.png',
    navbar: [{
      text: '首页',
      link: '/',
    }, {
      text: '文档',
      children: [{
        text: '开发指南',
        link: '/docs/installation.html'
      }, {
        text: 'Form组件',
        link: '/form/form-doc.html'
      }, {
        text: 'Crud组件',
        link: '/crud/crud-doc.html'
      }, {
        text: 'Data组件',
        link: '/data/data0.html'
      }, {
        text: 'Default其它组件',
        link: '/default/article.html'
      }, {
        text: '单组件',
        link: '/component/input.html'
      }]
    },
    {
      text: '商业版本',
      children: [{
        text: '授权购买 ',
        link: '/views/vip',
      }, {
        text: '授权查询',
        link: '/views/license'
      }, {
        text: '合作伙伴',
        link: '/docs/friend'
      },
        //  {
        //   text: '二次开发客户',
        //   link: '/docs/pirate'
        // }
      ]
    }, {
      text: '物料市场',
      children: [{
        text: '周边生态',
        link: '/plugins/avue-cli.html'
      }, {
        text: 'Avue Cloud公众号',
        link: '/views/article'
      }]
    },
    {
      text: '数据大屏',
      link: 'http://data.avuejs.com',
    },
    {
      text: '赞助支持',
      link: '/views/suporrt',
    },
    {
      text: '服务器1折起',
      children: [
        {
          text: '新用户专项优惠',
          link: 'https://promotion.aliyun.com/ntms/yunparter/invite.html?userCode=vqed4m0j',
        }, {
          text: '企业专项优惠',
          link: 'https://chuangke.aliyun.com/special/88?taskCode=shuangchuang_88_lb&recordId=876642&userCode=vqed4m0j',
        }]
    },
    {
      text: '个人支付接口',
      link: 'https://www.yungouos.com/#/invite/welcome?spm=MTA0MTc=',
    }],
    sidebar: {
      '/plugins': [
        '/plugins/avue-cli',
        '/plugins/ueditor-plugins'
      ],
      '/form': [
        "/form/form-doc",
        "/form/form-setup",
        "/form/form-object",
        "/form/form-input",
        "/form/form-number",
        "/form/form-select",
        "/form/form-cascader",
        "/form/form-checkbox",
        "/form/form-radio",
        "/form/form-date",
        "/form/form-time",
        "/form/form-switch",
        "/form/form-upload",
        "/form/form-title",
        "/form/form-array",
        "/form/form-dynamic",
        "/form/form-input-tree",
        "/form/form-input-icon",
        "/form/form-input-table",
        "/form/form-input-map",
        "/form/form-input-color",
        "/form/form-rate",
        "/form/form-slider",
        "/form/form-layout",
        "/form/form-rules",
        "/form/form-value",
        "/form/form-submit",
        "/form/form-slot",
        "/form/form-dic",
        "/form/form-cascader-item",
        "/form/form-data",
        "/form/form-event",
        "/form/form-ajax"
      ],
      '/crud': [
        "/crud/crud-doc",
        "/crud/crud-setup",
        "/crud/crud-object",
        "/crud/crud-page",
        "/crud/crud-search",
        "/crud/crud-head",
        "/crud/crud-row",
        "/crud/crud-column",
        "/crud/crud-dic",
        "/crud/crud-menu",
        "/crud/crud-fun",
        "/crud/crud-text",
        "/crud/crud-btn-slot",
        "/crud/crud-form",
        "/crud/crud-bind",
        "/crud/crud-sum",
        "/crud/crud-export",
        "/crud/crud-tree",
        "/crud/crud-children",
        "/crud/crud-cell",
        "/crud/crud-permission",
        "/crud/crud-rc",
        "/crud/crud-empty",
        "/crud/crud-loading",
        "/crud/crud-sortable",
        "/crud/crud-default",
        "/crud/crud-ajax",
        "/crud/crud-bigcousin",
        '/crud/api-crud-temp',
        '/crud/api-crud-fun',
      ],
      '/data': [
        '/data/data0',
        '/data/data1',
        '/data/data2',
        '/data/data3',
        '/data/data4',
        '/data/data5',
        '/data/data6',
        '/data/data7',
        '/data/data8',
        '/data/data9',
        '/data/data10',
        '/data/data11',
        '/data/data12',
      ],
      '/default': [
        "/default/article",
        "/default/count-up",
        "/default/comment",
        "/default/card",
        "/default/chat",
        "/default/contextmenu",
        "/default/clipboard",
        "/default/draggable",
        "/default/export",
        "/default/flow",
        "/default/image-preview",
        "/default/login",
        "/default/license",
        "/default/print",
        "/default/screenshot",
        "/default/search",
        "/default/sign",
        "/default/text-ellipsis",
        "/default/tabs",
        "/default/tree",
        "/default/video",
        "/default/verify",
        "/default/calendar",
        "/default/watermark"
      ],
      '/docs': [
        '/docs/home',
        '/docs/changelog',
        '/docs/contribution',
        '/docs/installation',
        '/docs/global',
        '/docs/locale',
        '/docs/api',
        '/docs/remote',
        '/docs/vip'
      ],
      '/component': [
        "/component/input",
        "/component/input-tree",
        "/component/input-number",
        "/component/input-color",
        "/component/input-icon",
        "/component/input-map",
        "/component/input-table",
        "/component/array",
        "/component/img",
        "/component/url",
        "/component/select",
        "/component/radio",
        "/component/checkbox",
        "/component/switch",
        "/component/datetime",
        "/component/time",
        "/component/rate",
        "/component/slider",
        "/component/cascader",
        "/component/upload"
      ],
    },
  }),
  plugins: [
    searchPlugin({
      // 配置项
    }),
    [
      blockDemo({
        path: __dirname,
      }),
    ],
  ],
  markdown: {
    lineNumbers: true,
  },
};
