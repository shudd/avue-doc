import { defineClientConfig } from "@vuepress/client";
import axios from 'axios'
import Bmob from './Bmob'
import website from './website'
import store2 from 'store2'
import './styles/index.scss'
import ElementPlus from "element-plus";
import zhCn from 'element-plus/dist/locale/zh-cn.js'
import "element-plus/dist/index.css";
import Avue from '@smallwei/avue'
import '@smallwei/avue/lib/index.css'

import Article from './components/Article.vue'
import Auth from './components/Auth.vue'
import Fp from './components/Fp.vue'
import License from './components/License.vue'
import Notice from './components/Notice.vue'
import Pay from './components/Pay.vue'
import PayList from './components/PayList.vue'
import PaySq from './components/PaySq.vue'
import Suporrt from './components/Suporrt.vue'
import Team from './components/Team.vue'
import Ueditor from './components/Ueditor.vue'
import Vip from './components/Vip.vue'


export default defineClientConfig({
  enhance ({ app }) {
    app.use(ElementPlus, {
      locale: zhCn,
    })
    app.config.globalProperties.website = website
    app.config.globalProperties.Bmob = Bmob
    app.config.globalProperties.store = store2
    app.component('Article', Article)
    app.component('Auth', Auth)
    app.component('Fp', Fp)
    app.component('License', License)
    app.component('Notice', Notice)
    app.component('Pay', Pay)
    app.component('PayList', PayList)
    app.component('PaySq', PaySq)
    app.component('Suporrt', Suporrt)
    app.component('Team', Team)
    app.component('Vip', Vip)
    app.component('Ueditor', Ueditor)
    app.use(Avue, {
      axios,
      cos: website.COS,
      qiniu: website.QINIU,
      ali: website.ALI
    })
  },
  setup () { },
  rootComponents: [Notice],
});
