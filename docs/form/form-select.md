# Select选择框

## 基础用法
:::demo 通过将`type`属性的值指定为`select`,同时配置`dicData`为字典值
form/form-select/base
:::


## 虚拟

>在某些使用情况下，单个选择器可能最终加载数万行数据。 将这么多的数据渲染至 DOM 中可能会给浏览器带来负担，从而造成性能问题。 为了更好的用户和开发者体验

:::demo  配置`virtualize`为`true`即可开启虚拟Dom模式
form/form-select/virtualize
:::




## 默认值

:::demo `value`属性可以提供一个初始化的默认值
form/form-select/value
:::

## 禁用状态

:::demo 通过`disabled`属性指定是否禁用
form/form-select/disabled
:::

## 禁用选项

:::demo 返回的字典中数据配置`disabled`属性指定是否禁用
form/form-select/disabled-item
:::

## 可清空

:::demo 使用`clearable`属性即可得到一个可清空的输入框,默认为`true`
form/form-select/clearable
:::

## 辅助语
:::demo 配置下拉数据中`desc`字段
form/form-select/desc
:::

## 字典属性
>指定标签文本和值，默认为label和value
:::demo 配置`props`属性来指定标签文本和值，默认为`label`和`value`
form/form-select/dic-local
:::

## 下拉框样式
```css
.popperClass .el-select-dropdown__item{
  background-color: rgba(0,0,0,.2);
}
```

:::demo `popperClass`属性配置样式的`class`名字,字典中`class`属性为单个框下拉样式
form/form-select/popperClass
:::


## 网络字典
>更多用法参考[表单数据字典](/form/form-dic)

:::demo 配置`dicUrl`指定后台接口的地址
form/form-select/dic-net
:::



## 基础多选

:::demo 设置`multiple`属性即可启用多选，此时值为当前选中值所组成的数组。默认情况下选中值会以 Tag 的形式展现，你也可以设置`collapseTags`属性将它们合并为一段文字,同时配合`maxCollapseTags`最大显示个数和`collapseTagsTooltip`是否折叠提示,`limit`限制选择个数
form/form-select/multiple
:::



## 创建条目和搜索
:::demo 使用`allowCreate`属性即可通过在输入框中输入文字来创建新的条目。注意此时`filterable`必须为真
form/form-select/search
:::

## 自定义模板

:::demo  配置`props`名称加`Type`卡槽开启即可自定义下拉框的内容,`typeformat`配置回显的内容,但是你提交的值还是`value`并不会改变
form/form-select/slot
:::



## 多级联动

:::demo  `cascader`为需要联动的子选择框`prop`值，填写多个就会形成一对多的关系
form/form-select/cascader
:::


## 远程搜索
>当你的下拉框数据量很大的时候，你可以启动远程搜索

:::demo  配置`remote`为`true`即可开启远程搜索，其中`dicUrl`中`'{{key}}'`为用户输入的关键字
form/form-select/remote
:::


## 分组


:::demo  配置`group`为`true`即可开启分组模式
form/form-select/group
:::



## 拖拽


``` html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<script src="https://cdn.staticfile.org/Sortable/1.10.0-rc2/Sortable.min.js"></script>
```


:::demo  配置`drag`为`true`即可开启拖拽模式
form/form-select/drag
:::



