# 表单默认值

## 配置方法
:::demo  配置`value`为字段默认值
form/form-value/value
:::

## 赋值方法
:::demo  利用`v-model`绑定的对象直接操作即可
form/form-value/model
:::
