# 表单验证


## 基础用法
:::tip
具体参考[async-validator](https://github.com/yiminghe/async-validator)
::::

:::demo  配置验证字段的`rules`的数据对象
form/form-rules/base
:::


## 外置验证

:::demo
form/form-rules/out
:::


## 自定义验证

:::demo  
form/form-rules/slot
:::

