# Tree树型选择框

## 基础用法

:::demo  
form/form-input-tree/base
:::

## 默认值

:::demo `value`属性可以提供一个初始化的默认值
form/form-input-tree/value
:::

## 禁用状态

:::demo 通过`disabled`属性指定是否禁用
form/form-input-tree/disabled
:::

## 禁用选项

:::demo 返回的字典中数据配置`disabled`属性指定是否禁用
form/form-input-tree/disabled-item
:::

## 可清空

:::demo 使用`clearable`属性即可得到一个可清空的输入框,默认为`true`
form/form-input-tree/clearable
:::

## 辅助语

:::demo  配置下拉数据中`desc`字段
form/form-input-tree/desc
:::

## 下拉框样式
```css
.popperClass .el-tree-node__content{
  background-color: rgba(0,0,0,.2);
}
```

:::demo `popperClass`属性配置样式的`class`名字
form/form-input-tree/popperClass
:::

## 字典属性
>指定标签文本和值，默认为label和value
:::demo 配置`props`属性来指定标签文本和值，默认为`label`和`value`
form/form-input-tree/dic
:::

## 网络字典
>更多用法参考[表单数据字典](/form/form-dic)

:::demo 配置`dicUrl`指定后台接口的地址
form/form-input-tree/dic-net
:::

## 基础多选

:::demo  设置`multiple`属性即可启用多选，此时值为当前选中值所组成的数组。默认情况下选中值会以 Tag 的形式展现，你也可以设置`collapseTags`属性将它们合并为一段文字,同时配合`maxCollapseTags`最大显示个数和`collapseTagsTooltip`是否折叠提示
form/form-input-tree/multiple
:::

## 选择任意级别

:::demo 当属性`checkStrictly`为`true` 时，任何节点都可以被选择，否则只有子节点可被选择。
form/form-input-tree/checkStrictly
:::




## 基础过滤

:::demo  filterable 属性即可启用筛选功能
form/form-input-tree/filter
:::

## 手风琴模式
:::demo  设置`accordion`对于同一级的节点，每次只能展开一个
form/form-input-tree/accordion
:::


## 节点事件

:::demo  
form/form-input-tree/node
:::





## 自定义模板

:::demo 配置`prop`名称加`Type`卡槽开启即可自定义下拉框的内容,`typeformat`配置回显的内容,但是你提交的值还是`value`并不会改变
form/form-input-tree/slot
:::


## 懒加载

:::demo  `cacheData`懒加载节点的缓存数据，结构与数据相同，用于获取未加载数据的标签
form/form-input-tree/lazy
:::

