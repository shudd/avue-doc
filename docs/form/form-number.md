
# Number数字输入框



## 基础用法

:::demo 通过将`type`属性的值指定为`number`
form/form-input/base
:::

## 默认值

:::demo `value`属性可以提供一个初始化的默认值
form/form-input/value
:::

## 禁用状态

:::demo `disabled`属性接受一个Boolean，设置为`true`即可禁用整个组件
form/form-number/disabled
:::

## 最大值最小值

:::demo 如果你只需要控制数值在某一范围内，可以设置`min`属性和`max`属性，不设置`min`和`max`时，最小值为 `0`。
form/form-number/max
:::



## 步数

:::demo 设置`step`属性可以控制步长，接受一个`Number`。
form/form-number/step
:::


## 严格步数

:::demo `stepStrictly`属性接受一个`Boolean`。如果这个属性被设置为`true`，则只能输入步数的倍数。
form/form-number/stepStrictly
:::


## 隐藏控制器

:::demo 设置`controls`属性是否使用控制按钮。
form/form-number/controls
:::

## 尺寸

:::demo 可通过`size`属性指定输入框的尺寸，默认为`small`，还提供了`large`,`small`和`mini`三种尺寸。
form/form-number/size
:::



## 精度

:::demo 设置`precision`属性可以控制数值精度，接收一个`Number`。
form/form-number/precision
:::


## 按钮位置

:::demo 设置 `controlsPosition`属性可以控制按钮位置。
form/form-number/controlsPosition
:::

