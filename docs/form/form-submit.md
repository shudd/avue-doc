# 表单操作按钮

## 提交按钮
:::demo  `submit`方法为表单提交按钮回调 
form/form-submit/submit
:::


## 清空按钮
:::demo  `reset-change`方法为表单清空按钮回调 
form/form-submit/reset
:::

## 隐藏按钮
:::demo  利用`submitBtn`和`emptyBtn`属性去隐藏按钮
form/form-submit/hide
:::

## 按钮文案
:::demo  利用`submitText`和`emptyText`属性去隐藏按钮
form/form-submit/text
:::

## 行内按钮

:::demo 利用列的`span`属性和`menuSpan`属性达到行内表单
form/form-submit/inline
:::

## 按钮位置
:::demo  利用`menuPosition`属性设置按钮的位置
form/form-submit/position
:::


## 自定义按钮
:::demo  利用`menu-form`卡槽去自定义按钮
form/form-submit/slot
:::


## 打印按钮
>你可以可以调用全局[$Print打印方法](/default/print)

:::demo  配置`printBtn`打开表单打印功能
form/form-submit/print
:::


## 模拟数据按钮
>一键生成模拟数据方便测试


``` html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Mock.js/1.0.0/mock-min.js"></script>
```

:::demo  `mock`设置`true`，在列中配置对应的规则即可，当然你可以自己写模拟逻辑，在`mock`写方法，会返回当前表单的数据,最后`return`即可，详情参考如下例子
form/form-submit/mock
:::



