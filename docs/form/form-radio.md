# Radio单选框


## 基础用法
>由于选项默认可见，不宜过多，若选项过多，建议使用 Select 选择器。

:::demo  通过将`type`属性的值指定为`radio`,同时配置`dicData`为字典值
form/form-radio/base
:::

## 网络字典
>更多用法参考[表单数据字典](/form/form-dic)

:::demo 配置`dicUrl`指定后台接口的地址
form/form-radio/dic
:::


## 默认值

:::demo `value`属性可以提供一个初始化的默认值
form/form-radio/value
:::

## 禁用状态

:::demo 通过`disabled`属性指定是否禁用
form/form-radio/disabled
:::

## 禁用选项

:::demo 返回的字典中数据配置`disabled`属性指定是否禁用
form/form-radio/disabled-item
:::




## 按钮样式
:::demo  配置`button`为`true`
form/form-radio/button
:::



## 空心样式
:::demo  配置`border`为`true`
form/form-radio/border
:::
