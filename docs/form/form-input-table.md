# Table表格选择器




## 基础用法 
>内部组件为crud组件，大部分属性参考Crud文档
:::demo
form/form-input-table/base
:::

## 默认值

:::demo `value`属性可以提供一个初始化的默认值
form/form-input-table/value
:::

## 多选

:::demo 设置`multiple`属性即可启用多选
form/form-input-table/multiple
:::


## 禁用状态

:::demo 通过`disabled`属性指定是否禁用
form/form-input-table/disabled
:::

## 与其它框交互
:::demo  利用内置的`getPropRef`方法可以获取内部值赋值给其它变量
form/form-input-table/default
:::


