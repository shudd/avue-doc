# Icon图标选择器


## 基础用法 
:::demo  图标集合参考例子配置`iconList`属性
form/form-input-icon/base
:::


## 默认值

:::demo `value`属性可以提供一个初始化的默认值
form/form-input-icon/value
:::

## 禁用状态

:::demo 通过`disabled`属性指定是否禁用
form/form-input-icon/disabled
:::

## 其他图标库 

```html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<!-- 引入阿里巴巴的图表库iconfont和svg图标后，直接使用图标名即可，记得加前缀 -->
<link rel="stylesheet" href="https://at.alicdn.com/t/font_567566_pwc3oottzol.css">
<script src="//at.alicdn.com/t/font_2621503_zcbiqy2g1i.js"></script>
```

:::demo
form/form-input-icon/default
:::
