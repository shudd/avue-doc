# Color颜色选择器

## 基础用法
:::demo  通过将`type`属性的值指定为`color`
form/form-input-color/base
:::


## 默认值

:::demo `value`属性可以提供一个初始化的默认值
form/form-input-color/value
:::

## 禁用状态

:::demo 通过`disabled`属性指定是否禁用
form/form-input-color/disabled
:::


## 颜色格式
:::demo  
form/form-input-color/format
:::

## 预定义颜色
:::demo  
form/form-input-color/color
:::
