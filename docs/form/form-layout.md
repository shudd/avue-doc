# 表单布局

:::tip
 - size
 - span
 - gutter
 - offset
 - labelWidth
 - labelPosition
  
 以上属性配置到`option`下作用于全部列，优先列中配置属性生效，更多用法可以参考[Element-plus-Layout 布局](https://element-plus.gitee.io/zh-CN/component/layout.html)和[Element-plus-Form 表单](https://element-plus.gitee.io/zh-CN/component/form.html)
::::

## 栏大小
:::demo  设置`size`属性调节栏的大小，默认为`small`
form/form-layout/size
:::

## 栏距列数
:::demo  设置`span`属性调节栏列数,默认为12
form/form-layout/span
:::

## 栏距间隔
:::demo  设置`gutter`属性调节栏列数,默认为0
form/form-layout/gutter
:::

## 分栏偏移
:::demo  设置`offset`属性调节栏列数,默认为12
form/form-layout/offset
:::

## 栏成行
:::demo  设置`row`属性栅格后面的内容是否从新的一行开始展示
form/form-layout/row
:::

## 栏排序

:::demo  设`order`属性可排序与`column`中顺序不同
form/form-layout/order
:::


## 栏隐藏
:::demo  设置`display`属性隐藏栏目
form/form-layout/display
:::


## 标题宽度


:::demo `labelWidth`为标题的宽度，默认为`110`
form/form-layout/labelWidth
:::

## 标题位置


:::demo `labelPosition`为标题的位置，默认为`left`
form/form-layout/labelPosition
:::

## 标题辅助语
:::demo  `labelTip`为标题提示的内容,`labelTipPlacement`为标题提示语的方向，默认为`bottom`
form/form-layout/labelTip
:::


## 内容辅助语
:::demo  `tip`为提示的内容,`tipPlacement`为提示语的方向，默认为`bottom`
form/form-layout/tip
:::


## 详情编辑

:::demo  `detail`控制是否为详情页
form/form-layout/detail
:::

:::demo  
form/form-layout/detail2
:::


## 分组展示
>将表单已分组的形式展示

:::demo  用法普通的form组件分组用法一样，在`group`中配置结构体即可
form/form-layout/group
:::


## 选项卡展示
>将表单已选项卡的形式展示

:::demo  配置`tabs`为true即可开启选项卡
form/form-layout/tabs
:::

