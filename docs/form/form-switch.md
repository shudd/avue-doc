# Switch开关

## 基础用法
>表示两种相互对立的状态间的切换，多用于触发「开/关」
:::demo 通过将`type`属性的值指定为`switch`,同时配置`dicData`为字典值
form/form-switch/base
:::


## 默认值

:::demo `value`属性可以提供一个初始化的默认值
form/form-switch/value
:::

## 禁用状态

:::demo 通过`disabled`属性指定是否禁用
form/form-switch/disabled
:::



## 字典属性
>指定标签文本和值，默认为label和value
:::demo 配置`props`属性来指定标签文本和值，默认为`label`和`value`
form/form-switch/dic
:::


## 网络字典
>更多用法参考[表单数据字典](/form/form-dic)

:::demo 配置`dicUrl`指定后台接口的地址，默认只会取前2项
form/form-switch/dic-net
:::


## 按钮颜色
:::demo 使用使用 CSS var `--el-switch-on-color`和`--el-switch-off-color`控制颜色。
form/form-switch/color
:::


## 图标
:::demo 使用`activeIcon`属性与`inactiveIcon`属性来设置状态的图标`inlinePrompt`属性可以让图标内置。使用`activeActionIcon`属性与`inactiveActionIcon`属性来设置按钮状态图标。当使用图标时，文字属性就不会展示
form/form-switch/icon
:::

## 阻止切换
:::demo 设置`beforeChange`函数回调done方法传入`true/false`
form/form-switch/loading
:::

