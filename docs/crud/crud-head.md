# 表头配置



## 固定表头
:::demo  设置`height`时当表格的高度超过设定值，就会出现滚动条，从而达到固定表头的效果
crud/crud-head/height
:::


## 隐藏表头


:::demo  设`showHeader`属性为`false`即可隐藏表头
crud/crud-head/showHeader
:::


## 多级表头
:::demo 只要在配置中添加children层级嵌套即可
crud/crud-head/children
:::


## 自定义列表头

:::demo 在卡槽中指定列的`prop`加上`-header`作为卡槽的名称即可自定义
crud/crud-head/headerslot
:::



## 自定义菜单栏左边

:::demo 卡槽为`menu-left`为表格菜单左边的位置
crud/crud-head/menu-left
:::

## 自定义菜单栏右边

:::demo  卡槽为`menu-right`为表格菜单右边的位置
crud/crud-head/menu-right
:::
