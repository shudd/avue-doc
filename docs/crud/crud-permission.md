# 权限控制

## 普通用法
:::demo
crud/crud-permission/base
:::

## 函数用法
:::demo
crud/crud-permission/fun
:::

## 自定义用法

:::tip
- [自定义按钮](/crud/crud-btn-slot.html#自定义弹窗内按钮)
- [按钮方法](/crud/crud-fun.html)
::::

:::demo
crud/crud-permission/slot
:::
