# 导入导出

``` html
<!-- 导入需要的包 （一定要放到index.html中的head标签里）-->
<script src="https://cdn.staticfile.org/FileSaver.js/2014-11-29/FileSaver.min.js"></script>
<script src="https://cdn.staticfile.org/xlsx/0.18.2/xlsx.full.min.js"></script>
```

## 表格导出
:::demo  `excelBtn`设置为`true`即可开启导出功能
crud/crud-export/export
:::


## 表格导入
:::demo  
crud/crud-export/import
:::



