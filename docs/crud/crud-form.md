# 弹窗表单配置

:::tip
 表格中的弹出的表单是内置组件`avue-form`组件,配置属性可以参考[FORM组件文档](/form/form)
::::

## 默认值
:::demo  配置`value`为字段默认值
crud/crud-form/value
:::

## 打开表单前

:::tip
你可以在打开前和关闭前做一些逻辑操作，例如打开根据id请求数据，关闭根据id保存数据等
::::

:::demo 打开表单前会执行`beforeOpen`方法，相关返回的方法值可以判断表单当前打开的类型是新增还是编辑
crud/crud-form/beforeOpen
:::

## 关闭表单前

:::demo 关闭表单前会执行`beforeClose`方法，执行返回的`done`方法后才会彻底关闭表单
crud/crud-form/beforeClose
:::


## 不关弹窗连续添加

:::tip
源码中涉及自定义卡槽和按钮的操作方法可以下面介绍
- [自定义按钮](/crud/crud-btn-slot.html#自定义弹窗内按钮)
- [按钮方法](/crud/crud-fun.html)
::::


:::demo
crud/crud-form/next
:::

## 表单按钮位置


:::demo 配置`dialogMenuPosition`属性值即可，默认为`right`
crud/crud-form/position
:::



## 打开表单方式



:::demo 配置`dialogType`为弹窗的方式,`dialogDirection`为弹窗的位置
crud/crud-form/type
:::



## 防重提交

:::demo  为了防止数据重复提交，加入了防重提交机制，`rowSave`方法和`rowUpdate`方法返回`done`用于关闭表单方法和`loading`用于关闭禁止的表单不关闭表单
crud/crud-form/done
:::


## 标题字段宽度

:::demo `labelWidth`为标题的宽度，默认为`90`，可以配置到`option`下作用于全部,也可以单独配置每一项
crud/crud-form/labelWidth
:::

## 验证

:::tip
具体参考[async-validator](https://github.com/yiminghe/async-validator)
::::

:::demo  配置验证字段的`rules`的数据对象
crud/crud-form/rules
:::


## 自定义验证

:::tip
 自定义校验 callback 必须被调用。 更多高级用法可参考 [async-validator](https://github.com/yiminghe/async-validator)。
::::

:::demo  
crud/crud-form/rules2
:::




## 组件对象


:::demo 打开表单的时候可以获取相关字段的ref对象
crud/crud-form/ref
:::


## 字段不同状态


:::demo `disabled`、`display`、`detail`等字段在新增和编辑不同状态下，字段的不同状态展示
crud/crud-form/status
:::


## 深结构绑定


:::demo  `bing`绑定深层次的结构对象，`prop`也是需要填写
crud/crud-form/bind
:::



## 字段排序


:::demo  配置`order`的序号可以实现表单和表格字段不同的顺序
crud/crud-form/order
:::


## 表单窗口拖拽


:::demo `dialogDrag`设置为`true`即可拖动表单
crud/crud-form/drag
:::


## 改变结构配置

:::demo  
crud/crud-form/defaults
:::

## 与其它字段交互

:::demo  
crud/crud-form/control
:::

## 表单分组

:::tip
- [表单分组详细用法](/form/form-group.html)
::::


:::demo  
crud/crud-form/group
:::



## 数据过滤

:::demo  `filterDic`设置为`true`返回的对象不会包含`$`前缀的字典翻译, `filterNull`设置为`true`返回的对象不会包含空数据的字段
crud/crud-form/filter
:::


## 自定义表单

:::demo 在卡槽中指定列的`prop`加上`-form`作为卡槽的名称开启自定义
crud/crud-form/formslot
:::


## 自定义表单错误提示

:::demo 在卡槽中指定列的`prop`加上`-error`作为卡槽的名称开启自定义
crud/crud-form/errorslot
:::


## 自定义表单标题

:::demo 在卡槽中指定列的`prop`加上`-label`作为卡槽的名称开启自定义
crud/crud-form/labelslot
:::