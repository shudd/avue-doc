# 表格高级用法


## 表格初始化

:::demo 
crud/crud-ajax/crud-reload
:::


## 配置项服务端加载
:::tip
- 这里没有走真真的服务器请求，而是做了一个模拟
::::

:::demo
crud/crud-ajax/base
:::


## 配置项切换

:::demo
crud/crud-ajax/change
:::
