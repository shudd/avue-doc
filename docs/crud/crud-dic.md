# 数据字典


:::tip
更多字典详细用法参考[Form组件数据字典](/form/form-dic.html)
``` js
//使用字典需要引入axios
import axios from 'axios'
const app =createApp({});
app.use(Avue,{axios});

```
::::




## 字典使用

:::demo 本地字典只要配置`dicData`为一个`Array`数组即可，便会自动加载字典到对应的组件中，注意返回的字典中value类型和数据的类型必须要对应，比如都是字符串或者都是数字。
crud/crud-dic/base
:::


## 字典联动

:::demo  `cascader`为需要联动的子选择框`prop`值，填写多个就会形成一对多的关系，表格加载联动数据需要调用内置方法`dicInit`
crud/crud-dic/cascader
:::