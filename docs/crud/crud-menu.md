# 操作栏配置


## 操作栏隐藏

:::demo `menu`属性接受一个`Boolean`的属性达到隐藏操作栏的效果，默认为`false`
crud/crud-menu/base
:::

## 操作栏对齐方式

:::demo `menuWidth`属性设置操作栏宽度,`menuTitle`属性设置操作栏目的文字,`menuAlign`属性设置对齐方式,`menuHeaderAlign`属性设置表头对齐方式
crud/crud-menu/menuWidth
:::

## 操作栏自适应
:::demo 通过`js`计算元素宽度，动态给`menuWidth`去赋值，实现动态宽度
crud/crud-menu/auto
:::

## 操作栏样式

:::demo `menuClassName`属性和`menuLabelClassName`属性配置操作栏列的单元格和表头样式名称
crud/crud-menu/className
:::

## 自定义操作栏头部

:::demo `menu-header`插槽为操作栏头部自定义
crud/crud-menu/slot
:::


## 自定义操作栏

:::demo `menu`为操作栏自定义
crud/crud-menu/headerSlot
:::



## 查看按钮
:::tip
 [自定义按钮](/crud/crud-btn-slot.html#自定义查看按钮)
::::

:::demo  `viewBtn`配置为`true`即可
crud/crud-menu/viewBtn
:::


## 复制按钮
:::demo 设置`copyBtn`为`true`时激活行复制功能,复制的数据会去除`rowKey`配置的主键
crud/crud-menu/copyBtn
:::

## 打印按钮

:::demo  `printBtn`设置为`true`即可开启打印功能
crud/crud-menu/printBtn
:::

## 导出按钮

``` html
<!-- 导入需要的包 （一定要放到index.html中的head标签里）-->
<script src="https://cdn.staticfile.org/FileSaver.js/2014-11-29/FileSaver.min.js"></script>
<script src="https://cdn.staticfile.org/xlsx/0.18.2/xlsx.full.min.js"></script>
```


:::demo `excelBtn`设置为`true`即可开启表格导出功能
crud/crud-menu/excelBtn
:::


## 筛选按钮

:::tip
 常用自定筛选条件
::::

:::demo  `filterBtn`默认为`true`，可以自定义过滤条件，根据`filter`函数回调
crud/crud-menu/filterBtn
:::


## 合并菜单

:::demo  配置`menuType`为`menu`表格的操作栏目菜单合并，`menuBtn`卡槽为自定义卡槽,
crud/crud-menu/menu
:::


## 图标菜单

:::demo  配置`menuType`为`icon`时表格操作栏为图标按钮
crud/crud-menu/icon
:::

