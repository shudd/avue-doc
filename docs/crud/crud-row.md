# 表格行配置项

## 边框

:::demo 默认情况下，是不具有竖直方向的边框的，如果需要，可以使用`border`属性，它接受一个`Boolean`，设置为`true`即可启用。
crud/crud-row/border
:::


## 条纹

:::demo 默认情况下，是不具有行彩色条纹的，如果需要，可以使用`stripe`属性，它接受一个`Boolean`，设置为`true`即可启用。
crud/crud-row/stripe
:::




## 行和单元格样式

:::demo 对开开放了`cell-style`和`row-style`方法
crud/crud-row/cell-style
:::


## 自定义行样式
```css
.warning-row{
  background-color: #F56C6C !important;
  color:#fff;
}
.success-row{
  background-color: #67C23A !important;
  color:#fff;
}
.warning-row.hover-row td,.success-row.hover-row td{
  background-color: initial !important;
}
```
:::demo 可以通过指定 组件的 `row-class-name` 属性来为 crud 中的某一行添加 class，表明该行处于某种状态,返回当前行的`row`数据和行的序号`index`
crud/crud-row/row-class-name
:::


## 行多选

:::demo  设`selection`属性为`true`即可；勾选的同时会回调`selectionChange`方法返回当前选中的数据,`toggleRowSelection`方法设置行勾选,`toggleAllSelection`方法设置全部勾选
crud/crud-row/selection
:::


## 禁止某个项选择

:::demo `selectable`函数决定该行是否可以勾选
crud/crud-row/selectable
:::


## 翻页多选

:::demo  设置`reserveSelection`为`true`保留之前的勾选
crud/crud-row/reserveSelection
:::

## 多选提示

:::demo 设置`tip`为`false`可以取消表格上方显示的提示，同时支持对应的卡槽自定义
crud/crud-row/tip
:::


## 行单选

:::demo 单选一行数据时需要设置`highlightCurrentRow`属性为`true`,回调`current-row-change`方法,同时返回当前行的`row`数据,
crud/crud-row/highlightCurrentRow
:::



## 行单选(利用卡槽)

:::demo  这里利用了列自定义卡槽方式去实现行单选
crud/crud-row/radio
:::



## 展开行

:::demo  使用`expand`属性时必须配置`rowKey`属性为你行数据的主键，不能重复, `defaultExpandAll`属性默认展开全部,`expandRowKeys`为展开指定`rowKey`主键的数组，同时你也可以调用`toggleRowExpansion`方法传入你要展开的`row`
crud/crud-row/expand
:::

## 展开行(手风琴模式)
:::demo  `expand-change`配置`expandRowKeys`去使用
crud/crud-row/expand-change
:::


## 行单击方法


:::demo 单击一行数据时回调`row-click`方法,同时返回当前行的`row`数据,`event`当前的操作对象,`column`当前列的属性
crud/crud-row/row-click
:::

## 行双击方法

:::demo 双击一行数据时回调`row-dblclick`方法,同时返回当前行的`row`数据,`event`当前的操作对象,`column`当前列的属性 
crud/crud-row/row-dblclick
:::

## 行拖拽排序

```html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<script src="https://cdn.staticfile.org/Sortable/1.10.0-rc2/Sortable.min.js"></script>
```

:::demo  `rowSort`设置为`true`即可开启拖拽功能，`sortable-change`为拖拽后的回调方法
crud/crud-row/drag
:::

## 行合并

:::tip
 如果数据不确定参考[动态数据行和列合并](/crud/crud-rc.html)
::::

:::demo 通过给传入`spanMethod`方法可以实现合并行或列，方法的参数是一个对象，里面包含当前行`row`、当前列`column`、当前行号`rowIndex`、当前列号`columnIndex`四个属性。该函数可以返回一个包含两个元素的数组，第一个元素代表`rowspan`，第二个元素代表`colspan`。 也可以返回一个键名为`rowspan`和`colspan`的对象。

crud/crud-row/spanMethod
:::


