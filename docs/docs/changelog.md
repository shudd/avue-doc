# 更新日志
:::tip
成为avue的贡献者，快来一起完善文档吧[文档源码](https://gitee.com/smallweigit/avue-doc)  
每次更新版本时候，记得看更新日志，小心踩坑～。～
::::

## 如果你对Avue比较有兴趣，可以联系客服加入我们一起开发Avue生态

<p>

## v3.3.1

<code>2023-01-26</code>

<code>优化</code>
- 优化Select和Upload组件的拖拽问题
- 优化底层编译打包问题

<code>修复</code>
- 修复Crud组件弹窗dialogDrag首次拖拽偏移问题[gitee_I8VPSP](https://gitee.com/smallweigit/avue/issues/gitee_I8VPSP)
- 修复Cascader验证不消失和emitPath属性不生效[gitee_I8RBCT](https://gitee.com/smallweigit/avue/issues/gitee_I8RBCT)
- 修复Crud组件搜索部分收缩高度自适应[gitee_I8VPV0](https://gitee.com/smallweigit/avue/issues/gitee_I8VPV0)
- 修复Select组件computed警告[gitee_I8TT3C](https://gitee.com/smallweigit/avue/issues/gitee_I8TT3C)
- 修复Crud组件拖拽动[gitee_I8TRFZ](https://gitee.com/smallweigit/avue/issues/gitee_I8TRFZ)
- 修复InputIcon组件清空无法使用[gitee_I8UI00](https://gitee.com/smallweigit/avue/issues/gitee_I8UI00)

</p>


<p>

## v3.3.0

<code>2024-01-02</code>

<code>新增</code>
- 新增Upload组件腾讯云COS存储[gitee_I69AAC](https://gitee.com/smallweigit/avue/issues/gitee_I69AAC),[在线例子](/form/form-upload/#腾讯云oss上传)
- 新增InputTabel组件多选和禁止参数[gitee_I6U771](https://gitee.com/smallweigit/avue/issues/gitee_I6U771)[在线例子](/form/form-input-table/#多选)
- 新增Select组件字典class属性[gitee_I7RDK4](https://gitee.com/smallweigit/avue/issues/gitee_I7RDK4),[在线例子](/form/form-select)


<code>修复</code>
- 修复Form组件control属性的底层问题
- 修复Upload组件查看时候的样式问题

</p>

<p>

## v3.2.23

<code>2023-12-01</code>

<code>修复</code>
- 修复Upload上传按钮查看时消失问题[gitee_I8F5SH](https://gitee.com/smallweigit/avue/issues/I8F5SH),[gitee_I8IF4L](https://gitee.com/smallweigit/avue/issues/I8IF4L)
- 修复Crud组件sort拖拽排序延时问题[gitee_I7CO53](https://gitee.com/smallweigit/avue/issues/I7CO53)
- 修复InputTabel组件的搜索分页问题[gitee_I8GERN](https://gitee.com/smallweigit/avue/issues/I8GERN)
- 修复Select组件virtualize卡槽和虚拟初始化问题
</p>

<code>新增</code>
- 新增Cascader组件getCheckedKeys方法

<p>

## v3.2.22

<code>2023-10-30</code>

<code>修复</code>
- 修复Form组件order属性失效问题[gitee_I8BF5O](https://gitee.com/smallweigit/avue/issues/I8BF5O)
- 修复Icon图标选择器el类名问题[gitee_I8BVHQ](https://gitee.com/smallweigit/avue/issues/I8BVHQ)
- 修复Crud组件formatter弹窗问题[gitee_I8BVA8](https://gitee.com/smallweigit/avue/issues/I8BVA8)

</p>


<p>

## v3.2.21

#### 同步最新element-plus(v2.4.0)
<code>2023-10-17</code>

<code>新增</code>
- 新增imagePreview图片预览interval定时参数[gitee_I85QL8](https://gitee.com/smallweigit/avue/issues/I85QL8),[在线例子](/default/image-preview.html)
- 新增tree组件nodeContextmenu右键事件[在线例子](/default/tree.html)
- 新增Crud组件参数问题
- 新增Date组件click事件
- 新增InputColor组件focus和blur事件

<code>修复</code>
- 修复Crud取消按钮cancelBtn的权限[gitee_I81RIB](https://gitee.com/smallweigit/avue/issues/I81RIB)
- 修复地图选择器支持高德地图2.0库[gitee_I81NGL](https://gitee.com/smallweigit/avue/issues/I81NGL)
- 修复Form组件error回调方法参数问题[gitee_I801VF](https://gitee.com/smallweigit/avue/issues/I801VF)
- 修复Upload上传组件样式问题[gitee_I844Q0](https://gitee.com/smallweigit/avue/issues/I844Q0)
- 修复Form组件tabs模式下错误问题[gitee_I86V0U](https://gitee.com/smallweigit/avue/issues/I86V0U)
- 修复dynamic卡槽不显示问题


</p>



<p>

## v3.2.19

<code>2023-8-10</code>

<code>新增</code>
- 新增Chat组件的menu功能卡槽[gitee_I7PTY4](https://gitee.com/smallweigit/avue/issues/I7PTY4)，[在线例子](/default/chat)

<code>修复</code>
- 修复数据字典0和""匹配问题[gitee_I73QXH](https://gitee.com/smallweigit/avue/issues/I7K9DT)
- 修复Form组件clearValidate偶尔调用为空问题[gitee_I7LQH1](https://gitee.com/smallweigit/avue/issues/I7LQH1)
- 修复Select字典的props配对问题[gitee_I7QPLS](https://gitee.com/smallweigit/avue/issues/I7QPLS)

</p>

<p>

## v3.2.17/v3.2.18

<code>2023-7-10</code>

<code>新增</code>
- 新增InputTree组件的drag拖拽方法[在线例子](/default/tree#权限)
- 新增Crud组件loading配置属性[在线例子](/crud/crud-loading)


<code>修复</code>
- 修复Flow组件的连线问题[gitee_I73QXH](https://gitee.com/smallweigit/avue/issues/I73QXH),[在线例子](/default/flow)
- 修复InputNumber组件stepStrictly树形失效问题[gitee_I73QXH](https://gitee.com/smallweigit/avue/issues/I73QXH)
- 修复Crud组件cascader联动不清空的问题

</p>

<p>

## v3.2.16

<code>2023-5-18</code>

<code>新增</code>
- 新增InputTree组件expandOnClickNode参数[gitee_I70OQ0](https://gitee.com/smallweigit/avue/issues/I70OQ0)


<code>修复</code>
- 修复ImagePreview组件type参数[gitee_I6Z0JY](https://gitee.com/smallweigit/avue/issues/I6Z0JY)
- 修复Crud组件excel导出问题[gitee_I702K9](https://gitee.com/smallweigit/avue/issues/I702K9)
- 修复Crud组件分组header插槽不生效问题[gitee_I6ZHX1](https://gitee.com/smallweigit/avue/issues/I6ZHX1)
- 修复Crud组件searchProp参数失效问题
- 修复TextElipsis组件失效问题

<code>优化</code>
- 优化InputTree组件checkStrictly参数默认值
- 优化展示核心方法和字典方法


</p>



<p>

## v3.2.15

<code>2023-4-22</code>

<code>新增</code>
- 新增Crud组件tooltipEffect属性和tooltipOptions属性[在线例子](/crud/crud-column.html#内容超出隐藏)
- 新增Crud组件和Form组件option属性对象模式[在线例子](/crud/crud-object),[在线例子](/form/form-object)


<code>修复</code>
- 修复Form组件control属性失效问题[在线例子](/form/form-event.html#组件交互)
- 修复Form组件gutter属性失效问题[gitee_I6VUCS](https://gitee.com/smallweigit/avue/issues/I6VUCS)
- 修复Crud组件导出的空问题[gitee_I6UZJF](https://gitee.com/smallweigit/avue/issues/I6UZJF)
- 修复Crud组件cell模式报错问题[在线例子](/crud/crud-cell.html)
- 修复Upload组件上传按钮disabled失效问题
- 修复字典res属性失效问题[在线例子](/form/form-dic.html#字段配置)


<code>优化</code>
- 优化国际化翻译
- 优化组件局部导出使用


</p>

<p>

## v3.2.14

<code>2023-4-7</code>


<code>修复</code>
- 修复Dynamic的内部问题
- 修复Crud复杂表头不显示问题

</p>


<p>

## v3.2.13

<code>2023-3-31</code>

<code>新增</code>
- 新增Crud组件的className单元格样式名称和labelClassName单元格标题样式名称[在线例子](/crud/crud-column.html#单元格和表头样式)
- 新增Crud组件expandClassName和expandLabelClassName展开列的单元格和标题样式名称
- 新增Crud组件selectionClassName和selectionLabelClassName多选列的单元格和标题样式名称
- 新增Crud组件indexClassName和indexLabelClassName序号列的单元格和标题样式名称
- 新增Crud组件menuLabelClassName和menuLabelClassName操作列的单元格和标题样式名称[在线例子](/crud/crud-menu.html#操作栏样式)
- 新增InputTree组件的filterNodeMethod方法[gitee_I6RGAW](https://gitee.com/smallweigit/avue/issues/I6RGAW)

<code>修复</code>
- 修复Dynamic组件的数据添加问题[gitee_I6R4AZ](https://gitee.com/smallweigit/avue/issues/I6R4AZ)
- 修复Crud组件和Form组件的Option数据赋值问题[gitee_I6QAC8](https://gitee.com/smallweigit/avue/issues/I6QAC8)
- 修复Crud组件excel导出合计统计问题
- 修复Crud组件summaryMethod方法的逻辑问题[gitee_I6K8DZ](https://gitee.com/smallweigit/avue/issues/I6K8DZ)
- 修复Form组件新增时Tabs增加问题
- 修复Upload组件的操作菜单问题
- 修复字典方法报错不抛出问题[gitee_I6QGRE](https://gitee.com/smallweigit/avue/issues/I6QGRE)
- 修复字典发送参数数组处理问题[gitee_I6NZON](https://gitee.com/smallweigit/avue/issues/I6NZON)

<code>优化</code>
- Crud组件和Form组件的内部逻辑优化
- 组件内部的一些样式调整

</p>

<p>

## v3.2.12

<code>2023-3-6</code>

<code>新增</code>
- 新增InputMap、InputIcon、InputColor、InputTable组件的prefixIcon和suffixIcon属性

<code>修复</code>
- 修复bind深结构绑定数据问题
- 修复字典深查找的问题

</p>

<p>

## v3.2.11

<code>2023-2-24</code>


<code>新增</code>
- 新增Dynamic组件支持upload-*相关函数逻辑
  
<code>修复</code>
- 修复Crud组件复杂表头默认全选下载
- 修复Crud组件和Form组件驼峰卡槽的不显示问题[gitee_I6FWTU](https://gitee.com/smallweigit/avue/issues/I6FWTU)
- 修复组件国际化问题
- 修复组件数据绑定问题
- 修复字典部分不显示问题[github_600](https://github.com/nmxiaowei/avue/issues/600)
- 修复字典数据为空的undefined问题
  
</p>


<p>

## v3.2.10

<code>2023-2-17</code>


<code>优化</code>
- 优化字典核心方法
- 优化工具类方法

<code>修复</code>
- 修复部分已知BUG
  
</p>

<p>

## v3.2.9

<code>2023-2-7</code>

<code>新增</code>
- 新增findNode和randomId全局方法[在线例子](/docs/api)

<code>优化</code>
- 优化Utils工具类的核心方法

<code>修复</code>
- 修复部分2.x遗留的BUG
- 修复Crud行编辑模式的卡槽问题
- 修复Crud行编辑模式下的验证表单问题[gitee_I6CORU](https://gitee.com/smallweigit/avue/issues/I6CORU)
  
</p>

