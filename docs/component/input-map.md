# inputMap地图选择器

```html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<!-- 高德地图api更新必须配合安全密钥使用 -->
<script>
  window._AMapSecurityConfig = {
    securityJsCode: 'xxxxx',
  }
</script>
<script type="text/javascript" src='https://webapi.amap.com/maps?v=2.0&key=xxxxxxxx&plugin=AMap.PlaceSearch'></script>
<script src="https://webapi.amap.com/ui/1.1/main.js?v=1.0.11"></script>
```

:::demo
component/input-map/index
:::