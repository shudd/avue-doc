# Search 搜索

:::tip
常用的表格或则列表的搜索组件，可以与form和crud等其他组件组合使用
::::

## 普通用法
:::demo  使用方法和常规用法一样`option`配置结构体，`v-model`为双向数据绑定，同时包含了一个`change`回调事件
default/search/index
:::

## Events

| 事件名 | 说明                       | 参数 |
| ------ | -------------------------- | ---- |
| change | 当搜索的值发生改变时的回调 | form |






