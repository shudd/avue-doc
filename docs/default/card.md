# Card 卡片



## 普通用法
:::demo
default/card/index
:::


## Variables

| 参数   | 说明                               | 类型   | 可选值 | 默认值 |
| ------ | ---------------------------------- | ------ | ------ | ------ |
| option | 组件配置属性，详情见下面Option属性 | Object | —      | —      |
| data   | 显示的数据                         | Array  | —      | —      |



## Option Attributes

| 参数   | 说明       | 类型    | 可选值     | 默认值 |
| ------ | ---------- | ------- | ---------- | ------ |
| addBtn | 添加按钮   | Boolean | true/false | true   |
| span   | 表单栅列   | Number  | -          | 8      |
| gutter | 项之间的间 | Number  | -          | 20     |


## Props Attributes

| 参数  | 说明         | 类型   | 可选值 | 默认值 |
| ----- | ------------ | ------ | ------ | ------ |
| title | 列表的主标题 | String | —      | title  |
| img   | 列表的头像   | String | —      | img    |
| info  | 列表的副标题 | String | —      | info   |


## Events

| 事件名    | 说明                         | 参数      |
| --------- | ---------------------------- | --------- |
| row-add   | 新增数据后点击确定触发该事件 | -         |
| row-click | 当某一行被点击时会触发该事件 | row,index |

## Scoped Slot

| name | 说明                            |
| ---- | ------------------------------- |
| menu | 操作栏目自定义参数为{row,index} |


