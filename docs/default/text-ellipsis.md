# TextEllipsis 超出文本省略

当问题太多时可以只展示部分，后面省略显示


## 基本调用
:::demo
default/text-ellipsis/index
:::

## 自定义前缀后缀
:::demo
default/text-ellipsis/slot
:::


## 当被隐藏文字的时候，使用tooltip提示
:::demo
default/text-ellipsis/tip
:::



## Variables

| 参数            | 说明               | 类型    | 可选值 | 默认值 |
| --------------- | ------------------ | ------- | ------ | ------ |
| text            | 需要省略的文本     | String  | -      | -      |
| width           | 限制的宽           | Number  | -      | -      |
| height          | 限制的高           | Number  | -      | -      |
| is-limit-height | 是否开启限制       | Boolean | -      | true   |
| use-tooltip     | 是否使用tooltip    | Boolean | -      | false  |
| placement       | tooltip的placement | String  | -      |


## Events
| 事件名 | 说明                                      | 参数 |
| ------ | ----------------------------------------- | ---- |
| show   | 当isLimitHeight为true，文本全部展示的时候 | -    |
| hide   | 当isLimitHeight为true，文本省略的时候     | -    |


