# Watermark 水印



:::demo
default/watermark/index
:::



## Variables

| 参数      | 说明         | 类型   | 可选值 | 默认值                 |
| --------- | ------------ | ------ | ------ | ---------------------- |
| text      | 文字内容     | String | -      | avue商用通用无敌大水印 |
| fontSize  | 字体大小     | String | -      | 30px                   |
| fontStyle | 字体类型     | String | -      | 黑体                   |
| width     | 字体的宽度   | String | -      | 200                    |
| height    | 字体的高度   | String | -      | 400                    |
| color     | 字体颜色     | String | -      | rgba(100,100,100,0.15) |
| degree    | 文本旋转角度 | String | -      | -20                    |