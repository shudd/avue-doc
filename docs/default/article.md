
# Article 文章
普通的文档详情页


:::demo
default/article/index
:::


## Variables

| 参数       | 说明                             | 类型   | 可选值 | 默认值 |
| ---------- | -------------------------------- | ------ | ------ | ------ |
| id         | 指定父类                         | String | -      | window |
| offset-top | 距离窗口顶部达到指定偏移量后触发 | Number | -      | 0      |



