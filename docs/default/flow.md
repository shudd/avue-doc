# Flow 流程
常用的流程组件

``` html
<!-- 导入需要的包 （一定要放到index.html中的head标签里-->
<script src="https://cdn.staticfile.org/jsPlumb/2.11.1/js/jsplumb.min.js"></script>
```

:::demo 点击线条即可删除线条
default/flow/index
:::


## Variables

| 参数   | 说明     | 类型   | 可选值 | 默认值 |
| ------ | -------- | ------ | ------ | ------ |
| width  | 画布宽度 | Number | -      | -      |
| height | 画布高度 | Number | -      | -      |
| option | 属性配置 | Object | -      | -      |

## Events

| 事件名 | 说明         | 参数 |
| ------ | ------------ | ---- |
| click  | 节点点击事件 | node |

## Methods

| 方法名     | 说明     | 参数 |
| ---------- | -------- | ---- |
| nodeAdd    | 节点新增 | name |
| deleteNode | 节点删除 | id   |
